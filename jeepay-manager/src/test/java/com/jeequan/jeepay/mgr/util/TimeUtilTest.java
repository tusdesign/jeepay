package com.jeequan.jeepay.mgr.util;

import org.apache.commons.lang3.tuple.MutablePair;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TimeUtilTest {

    @Test
    void getYearRange() {

        MutablePair<String, String> yearPair= TimeUtil.getYearRange(2023);
        Assert.isTrue(yearPair.left.equals("2023-01-01 00:00:00")&& yearPair.right.equals("2023-12-31 23:59:59"), "test successfully");
    }

    @Test
    void getMonthRange() {

        MutablePair<String, String> yearPair= TimeUtil.getMonthRange(2023,4);
        Assert.isTrue(yearPair.left.equals("2023-04-01 00:00:00")&& yearPair.right.equals("2023-04-30 23:59:59"), "test successfully");
    }

    @Test
    void getWeekRange() {

        MutablePair<String, String> yearPair= TimeUtil.getWeekRange(2023,4);
        Assert.isTrue(yearPair.left.equals("2023-01-23 00:00:00")&& yearPair.right.equals("2023-01-29 23:59:59"), "test successfully");
    }

    @Test
    void getDayRange() {

        MutablePair<String, String> yearPair= TimeUtil.getDayRange(2023,4,25);
        Assert.isTrue(yearPair.left.equals("2023-04-25 00:00:00")&& yearPair.right.equals("2023-04-25 23:59:59"), "test successfully");
    }
}