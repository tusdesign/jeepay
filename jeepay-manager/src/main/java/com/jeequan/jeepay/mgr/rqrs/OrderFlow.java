package com.jeequan.jeepay.mgr.rqrs;

import lombok.Data;

import java.util.Date;


@Data
public class OrderFlow {


    private String payOrderId;

    /**
     * 商户号
     */
    private String mchNo;

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 商户名称
     */
    private String mchName;

    /**
     * 商户账单金额,单位分
     */
    private Double amount;


    private String pid;

    /**
     * 业务id
     */
    private String businessId;

    /**
     * 交易类型：PERSONAL-个人支付,DEPARTMENTAL-部门支付
     */
    private String dealType;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 支付结果
     */
    private String payResult;
}