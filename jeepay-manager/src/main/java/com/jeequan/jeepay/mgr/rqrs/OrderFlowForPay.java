package com.jeequan.jeepay.mgr.rqrs;

import lombok.Data;

import java.util.Date;
import java.util.List;


@Data
public class OrderFlowForPay {


    private String typeName;

    /**
     * 每种支付方式对应的总账
     */
    private double totalAccountForDealType;

    /**
     * 订单集合
     */
    private List<OrderFlow> orderFlowList;
}