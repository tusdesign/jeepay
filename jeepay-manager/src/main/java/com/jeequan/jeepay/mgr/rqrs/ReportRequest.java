package com.jeequan.jeepay.mgr.rqrs;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ReportRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private int requestType;

    /**
     * 账单分类：按年度，按月度，按周，按日
     */
    private String reportType;

    /**
     * 年份
     */
    private int year;

    /**
     * 月份
     */
    private int month;

    /**
     * 一年当中的某一周
     */
    private int weekOfYear;

    /**
     * 某个月的某一天
     */
    private int dayOfMonth;

    /**
     * 订单开始创建时间
     */
    private String timeStart;

    /**
     * 订单结束创建时间
     */
    private String timeEnd;


    /**
     * 交易类型
     */
    private String dealTypes;


    /**
     * 应用列表
     */
    private String appArray;

}
