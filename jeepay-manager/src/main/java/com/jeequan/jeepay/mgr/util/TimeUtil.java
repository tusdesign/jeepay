package com.jeequan.jeepay.mgr.util;

import lombok.SneakyThrows;
import org.apache.commons.lang3.tuple.MutablePair;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Date;

@Component
public class TimeUtil {

    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 获取昨天0点0分0秒的时间
     */
    @SneakyThrows
    public static String getBeforeFirstDayDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        System.out.println("当前星期(日期)：" + format.format(calendar.getTime()));
        calendar.add(Calendar.DATE, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 00);//将小时至00
        calendar.set(Calendar.MINUTE, 00);//将分钟至00
        calendar.set(Calendar.SECOND, 00);//将秒至00
        String timeString = format.format(calendar.getTime());
        return timeString;
    }

    /**
     * 获取昨天天23点59分59秒的时间
     */
    @SneakyThrows
    public static String getBeforeLastDayDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);//将小时至23
        calendar.set(Calendar.MINUTE, 59);//将分钟至59
        calendar.set(Calendar.SECOND, 59); //将秒至59
        String timeString = format.format(calendar.getTime());
        return timeString;
    }


    /**
     * 获取上一周1号0点0分0秒的时间
     */
    @SneakyThrows
    public static String getBeforeFirstWeekDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.WEEK_OF_YEAR, -1);
        System.out.println("上周星期(日期)：" + format.format(calendar.getTime()));
        calendar.add(Calendar.DAY_OF_WEEK, 0);
        calendar.set(Calendar.DAY_OF_WEEK, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 00);
        calendar.set(Calendar.MINUTE, 00); //将分钟至00
        calendar.set(Calendar.SECOND, 00);//将秒至00
        String timeString = format.format(calendar.getTime());
        return timeString;
    }

    /**
     * 获取上一周最后一天23点59分59秒的时间
     */
    @SneakyThrows
    public static String getBeforeLastWeekDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.WEEK_OF_YEAR, -1);
        System.out.println("上周星期(日期)：" + format.format(calendar.getTime()));
        calendar.set(Calendar.HOUR_OF_DAY, 23);//将小时至23
        calendar.set(Calendar.MINUTE, 59);  //将分钟至59
        calendar.set(Calendar.SECOND, 59); //将秒至59
        String timeString = format.format(calendar.getTime());
        return timeString;
    }

    /**
     * 获取上一个月1号0点0分0秒的时间
     */
    @SneakyThrows
    public static String getBeforeFirstMonthDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 00);//将小时至00
        calendar.set(Calendar.MINUTE, 00);//将分钟至00
        calendar.set(Calendar.SECOND, 00);  //将秒至00
        String timeString = format.format(calendar.getTime());
        return timeString;
    }


    /**
     * 获取上个月的最后一天23点59分59秒的时间
     */
    @SneakyThrows
    public static String getBeforeLastMonthDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
        calendar.set(Calendar.MINUTE, 59); //将分钟至59
        calendar.set(Calendar.SECOND, 59);//将秒至59
        String timeString = format.format(calendar.getTime());
        return timeString;
    }

    /**
     * 获取上年1号0点0分0秒的时间
     */
    @SneakyThrows
    public static String getBeforeFirstYearDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, -1);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 00);//将小时至00
        calendar.set(Calendar.MINUTE, 00);//将分钟至00
        calendar.set(Calendar.SECOND, 00);  //将秒至00
        String timeString = format.format(calendar.getTime());
        return timeString;
    }


    /**
     * 获取上年的最后一天23点59分59秒的时间
     */
    @SneakyThrows
    public static String getBeforeLastYearDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, -1);
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        calendar.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
        calendar.set(Calendar.MINUTE, 59); //将分钟至59
        calendar.set(Calendar.SECOND, 59);//将秒至59
        String timeString = format.format(calendar.getTime());
        return timeString;
    }

    //获取相应年月下的天数
    @SneakyThrows
    public static int getDaysByYearMonth(int year, int month) {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    //输入你想要的年份得到该年的开始时间和结束时间
    @SneakyThrows
    public static MutablePair<String, String> getYearRange(int year){

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH, calendar.getActualMinimum(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 00);//将小时至00
        calendar.set(Calendar.MINUTE, 00);//将分钟至00
        calendar.set(Calendar.SECOND, 00);  //将秒至00
        String timeString1 = format.format(calendar.getTime());

        calendar.set(Calendar.MONTH, calendar.getActualMaximum(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
        calendar.set(Calendar.MINUTE, 59); //将分钟至59
        calendar.set(Calendar.SECOND, 59);//将秒至59
        String timeString2 = format.format(calendar.getTime());

        return MutablePair.of(timeString1, timeString2);
    }

    //输入你想要的年份和月数得到该月的开始时间和结束时间
    @SneakyThrows
    public static MutablePair<String, String> getMonthRange(int year, int month){

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH, month-1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 00);//将小时至00
        calendar.set(Calendar.MINUTE, 00);//将分钟至00
        calendar.set(Calendar.SECOND, 00);  //将秒至00
        String timeString1 = format.format(calendar.getTime());

        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
        calendar.set(Calendar.MINUTE, 59); //将分钟至59
        calendar.set(Calendar.SECOND, 59);//将秒至59
        String timeString2 = format.format(calendar.getTime());

        return MutablePair.of(timeString1, timeString2);
    }

    //输入你想要的年份和周数得到该周的开始时间和结束时间
    @SneakyThrows
    public static MutablePair<String, String> getWeekRange(int year, int weekOfYear) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // 创建一个Calendar实例
        Calendar calendar = Calendar.getInstance();
        calendar.setWeekDate(year,weekOfYear, 1);

        // 设置日期为当前时间
        //calendar.setTimeInMillis(System.currentTimeMillis());

        // 获取当前周的开始时间
        calendar.set(Calendar.DAY_OF_WEEK, -(calendar.get(calendar.getFirstDayOfWeek())-2));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date startOfWeek = calendar.getTime();

        // 获取当前周的结束时间
        calendar.add(Calendar.DAY_OF_WEEK, 6);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        Date endOfWeek = calendar.getTime();

        return MutablePair.of(format.format(startOfWeek), format.format(endOfWeek));
    }

    //输入你想要的年份和月数和所在天数得到当天的开始时间和结束时间
    @SneakyThrows
    public static MutablePair<String, String> getDayRange(int year, int month, int day){

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH, month-1);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, 00);//将小时至00
        calendar.set(Calendar.MINUTE, 00);//将分钟至00
        calendar.set(Calendar.SECOND, 00);  //将秒至00
        String timeString1 = format.format(calendar.getTime());

        calendar.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
        calendar.set(Calendar.MINUTE, 59); //将分钟至59
        calendar.set(Calendar.SECOND, 59);//将秒至59
        String timeString2 = format.format(calendar.getTime());

        return MutablePair.of(timeString1, timeString2);
    }

}
