/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeequan.jeepay.core.constants;

/*
 * 接口返回码
 *
 * @author terrfly
 * @site https://www.jeequan.com
 * @date 2021/5/24 17:07
 */
public enum PaySourceEnum {

    ALI_PAY("ALI_PAY", "支付宝"),
    WECHAT_PAY("WECHAT_PAY", "微信"),
    CARD_PAY("CARD_PAY", "银行卡"),
    P_WALLET_PAY("P_WALLET_PAY", "个人钱包"),
    A_WALLET_PAY("A_WALLET_PAY", "补贴钱包"),
    MIX_PAY("MIX_PAY", "混合支付"),
    CHINA_PAY("CHINA_PAY", "银联在线"),
    YSF_PAY("YSF_PAY", "云闪付"),
    OTHER_PAY("OTHER_PAY", "其它支付");

    private String code;

    private String msg;

    PaySourceEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }


    public static PaySourceEnum getPaySourceByKey(String key) {
        for (PaySourceEnum sourceEnum : PaySourceEnum.values()) {
            if (sourceEnum.getCode().equalsIgnoreCase(key)) {
                return sourceEnum;
            }
        }
        return PaySourceEnum.OTHER_PAY;
    }
}
