package com.jeequan.jeepay.core.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PayOrderFlow  implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * 支付订单号
     */
    private String payOrderId;

    /**
     * 商户订单号
     */
    private String mchOrderNo;

    /**
     * 商户号
     */
    private String mchNo;

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 应用名称
     */
    private String appName;


    /**
     * 应用下子系统分类
     */
    @TableField("ext_type")
    private String extType;

    /**
     * 商户名称
     */
    private String mchName;

    /**
     * 商户账单金额,单位分
     */
    private Double amount;


    /**
     * 商户账单金额,单位分
     */
    private Double refundAmount;

    /**
     * 用户id
     */
    private String pid;

    /**
     * 业务id
     */
    private String businessId;

    /**
     * 部门id
     */
    private String deptId;

    /**
     * 交易类型：PERSONAL-个人支付,DEPARTMENTAL-部门支付
     */
    private String dealType;

    /**
     * 支付通道
     */
    private String wayCode;

    /**
     * 商品标题
     */
    private String subject;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 支付结果
     */
    private String payResult;


    /**
     * 支付方式
     */
    private String paySource;
}
