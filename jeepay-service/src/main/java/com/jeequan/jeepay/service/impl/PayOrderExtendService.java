package com.jeequan.jeepay.service.impl;

import com.jeequan.jeepay.core.entity.OrderStatisticsDept;
import com.jeequan.jeepay.core.entity.PayOrderExtend;
import com.jeequan.jeepay.core.entity.PayOrderFlow;
import com.jeequan.jeepay.service.mapper.PayOrderExtendMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 支付订单表 服务实现类
 * </p>
 *
 * @author [mybatis plus generator]
 * @since 2023-02-03
 */
@Service
public class PayOrderExtendService extends ServiceImpl<PayOrderExtendMapper, PayOrderExtend> {

    @Autowired
    private PayOrderExtendMapper payOrderExtendMapper;

}
