/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeequan.jeepay.service.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.jeequan.jeepay.core.entity.PayOrderFlow;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.mapping.ResultSetType;
import org.apache.ibatis.session.ResultHandler;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 流水订单表 Mapper 接口
 * </p>
 *
 * @author [mybatis plus generator]
 * @since 2021-04-27
 */
public interface PayOrderFlowMapper extends BaseMapper<PayOrderFlow> {

    /** 租户收账情况 **/
    List<PayOrderFlow> selectMerchantOrder(Map param);

    /** 租户收账统计 **/
    List<PayOrderFlow> selectMerchantAccount(Map param);

    /** 查询数量 **/
    Integer selectOrderFlowCount(@Param(Constants.WRAPPER) Wrapper<?> queryWrapper);

    /** 订单流水导出 **/
    @Options(resultSetType = ResultSetType.FORWARD_ONLY, fetchSize = Integer.MIN_VALUE)
    void orderStreamQuery(@Param(Constants.WRAPPER) Wrapper<?> queryWrapper, ResultHandler<PayOrderFlow> handler);
}
