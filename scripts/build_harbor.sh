export TZ=Asia/Shanghai
export BUILD_TIME=$(date +'%Y-%m-%dT%H:%M:%S%z')
if [ "$HARBOR_URL" = "" ]; then
    HARBOR_URL="oci.tuxm.art:8443"
fi

if [ "$TAG" = "" ]; then
    TAG="latest"
fi

export DOCKER_DEFAULT_PLATFORM=linux/amd64

# docker build -t jeepay-deps:latest -f docs/Dockerfile_github .

docker buildx build -f Dockerfile_github \
	-t ${HARBOR_URL}/tusdesign/jeepay-payment:${TAG} \
    --build-arg USER=${GITHUB_USER} \
    --build-arg TOKEN=${GITHUB_TOKEN} \
    --build-arg PORT=9216 \
    --build-arg PLATFORM=payment \
    --build-arg BUILD_TIME=${BUILD_TIME} \
    --build-arg VERSION=${VERSION} \
    --build-arg COMMIT_SHA=${COMMIT_SHA} .

docker buildx build -f Dockerfile_github \
 -t ${HARBOR_URL}/tusdesign/jeepay-manager:${TAG} \
 --build-arg PORT=9217 \
 --build-arg PLATFORM=manager \
 --build-arg BUILD_TIME=${BUILD_TIME} \
 --build-arg VERSION=${VERSION} \
 --build-arg COMMIT_SHA=${COMMIT_SHA} .

docker buildx build -f Dockerfile_github \
 -t ${HARBOR_URL}/tusdesign/jeepay-merchant:${TAG} \
 --build-arg PORT=9218 \
 --build-arg PLATFORM=merchant \
 --build-arg BUILD_TIME=${BUILD_TIME} \
 --build-arg VERSION=${VERSION} \
 --build-arg COMMIT_SHA=${COMMIT_SHA} .

docker login ${HARBOR_URL} --username ${HARBOR_USER} --password ${HARBOR_PASS}
docker push ${HARBOR_URL}/tusdesign/jeepay-payment:${TAG}
docker push ${HARBOR_URL}/tusdesign/jeepay-manager:${TAG}
docker push ${HARBOR_URL}/tusdesign/jeepay-merchant:${TAG}

