export DOCKER_DEFAULT_PLATFORM=linux/amd64

if [ "$HARBOR_URL" = "" ]; then
    HARBOR_URL="oci.tuxm.art:8443"
fi

if [ "$TAG" = "" ]; then
    TAG="latest"
fi

IMAGE_TAG=${HARBOR_URL}/tusdesign/jeepay-activemq:${TAG}

docker build -t ${IMAGE_TAG} ./docker/activemq
docker push ${IMAGE_TAG}