export DOCKER_DEFAULT_PLATFORM=linux/amd64

if [ "$HARBOR_URL" = "" ]; then
    HARBOR_URL="oci.tuxm.art:8443"
fi

if [ "$TAG" = "" ]; then
    TAG="latest"
fi

docker build -t $HARBOR_URL/tusdesign/jeepay-deps:${TAG} -f docs/Dockerfile .
docker push $HARBOR_URL/tusdesign/jeepay-deps:${TAG}
